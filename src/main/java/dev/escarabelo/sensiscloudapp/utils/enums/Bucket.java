package dev.escarabelo.sensiscloudapp.utils.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Bucket {
    FILE_UPLOAD("scenariocloudbucket");
    private final String bucketName;
}
