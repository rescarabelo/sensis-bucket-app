package dev.escarabelo.sensiscloudapp.services.file;

import dev.escarabelo.sensiscloudapp.database.dao.FileDAO;
import dev.escarabelo.sensiscloudapp.database.models.File;
import dev.escarabelo.sensiscloudapp.services.S3Service;
import dev.escarabelo.sensiscloudapp.services.mail.EmailService;
import dev.escarabelo.sensiscloudapp.utils.enums.Bucket;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

@Service
@AllArgsConstructor
public class FileService implements FileServiceInterface{
    private final S3Service s3Service;
    private final FileDAO fileDAO;
    private final EmailService emailService;

    @Override
    public File saveFile(MultipartFile file) {
        if (file.isEmpty()) {
            throw new IllegalStateException("Impossível salvar arquivo vazio.");
        }

        Map<String, String> fileMetadata = new HashMap<>();
        fileMetadata.put("Content-Type",file.getContentType());
        fileMetadata.put("Content-Length",String.valueOf(file.getSize()));

        String path = String.format("%s/%s", Bucket.FILE_UPLOAD.getBucketName(), UUID.randomUUID());
        String filename = String.format("%s", file.getOriginalFilename());

        try {
            s3Service.uploadFile(path, filename, Optional.of(fileMetadata), file.getInputStream());
        } catch (Exception e) {
            throw new IllegalStateException("Erro ao salvar arquivo.", e);
        }

        File zip = File.builder().filename(filename).filePath(path).sentAt(Timestamp.from(Instant.from(ZonedDateTime.now(ZoneId.of("America/Sao_Paulo"))))).build();

        fileDAO.save(zip);
        emailService.sendMessage("r.escarabelo@gmail.com", "Arquivo enviado para o bucket com sucesso!",
                """
                Por favor não responda a esta mensagem.
                
                Estou aqui para avisar que um arquivo foi enviado pela aplicação feita por Rafael Escarabelo.
                
                Grato,
                Bot.
                """
                );

        return zip;
    }

    @Override
    public List<File> getAllFiles() {
        List<File> files = new ArrayList<>();
        fileDAO.findAll().forEach(files::add);
        return files;
    }
}
