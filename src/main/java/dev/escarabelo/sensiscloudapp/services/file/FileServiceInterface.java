package dev.escarabelo.sensiscloudapp.services.file;

import dev.escarabelo.sensiscloudapp.database.models.File;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileServiceInterface {
    File saveFile(MultipartFile file);

    List<File> getAllFiles();
}
