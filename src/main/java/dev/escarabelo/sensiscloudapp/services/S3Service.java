package dev.escarabelo.sensiscloudapp.services;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

@Service
public class S3Service {
    private final AmazonS3Client s3Client;

    public S3Service(AmazonS3Client s3Client) {
        this.s3Client = s3Client;
    }

    public void uploadFile(
            String path,
            String filename,
            Optional<Map<String, String>> metaData,
            InputStream inputStream
    ) throws Exception {
        ObjectMetadata objMetaData = new ObjectMetadata();
        metaData.ifPresent(map -> {
            if (!map.isEmpty()) {
                map.forEach(objMetaData::addUserMetadata);
            }
        });
        try {
            s3Client.putObject(path, filename, inputStream, objMetaData);
        } catch (AmazonServiceException err) {
            throw new Exception("Failed to upload File", err);
        }
    }

    public byte[] download(String path, String key) {
        try {
            S3Object object = s3Client.getObject(path, key);
            S3ObjectInputStream objectContent = object.getObjectContent();
            return IOUtils.toByteArray(objectContent);
        } catch (AmazonServiceException | IOException e) {
            throw new IllegalStateException("Failed to download the file", e);
        }
    }
}
