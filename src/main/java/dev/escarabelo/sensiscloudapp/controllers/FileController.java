package dev.escarabelo.sensiscloudapp.controllers;

import dev.escarabelo.sensiscloudapp.database.models.File;
import dev.escarabelo.sensiscloudapp.services.file.FileService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@CrossOrigin("*")
@AllArgsConstructor
@RequestMapping("api/v1/s3")
@RestController
public class FileController {
    FileService fileService;

    @GetMapping
    public ResponseEntity<List<File>> getTodos() {
        return new ResponseEntity<>(fileService.getAllFiles(), HttpStatus.OK);
    }

    @PostMapping(
            path = "",
            consumes = MULTIPART_FORM_DATA_VALUE
    )
    public ResponseEntity<File> saveFile(
            @RequestParam("file") MultipartFile file
            ) {
        return new ResponseEntity<>(fileService.saveFile(file), HttpStatus.OK);
    }
}
