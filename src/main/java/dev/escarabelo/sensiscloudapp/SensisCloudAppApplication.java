package dev.escarabelo.sensiscloudapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SensisCloudAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(SensisCloudAppApplication.class, args);
    }
}
