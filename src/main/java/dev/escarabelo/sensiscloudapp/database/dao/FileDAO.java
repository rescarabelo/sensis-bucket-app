package dev.escarabelo.sensiscloudapp.database.dao;

import dev.escarabelo.sensiscloudapp.database.models.File;
import org.springframework.data.repository.CrudRepository;

public interface FileDAO extends CrudRepository<File, Long> {
    File findFileByFilename(String fl);
}
